from flask import Flask, jsonify, request, testing

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello Bob from minseo'

@app.route('/add')
def add():
    if request.method == 'GET':
        a = request.args.get('a')
        b = request.args.get('b')
        return str(int(a)+int(b))

@app.route('/sub')
def sub():
    if request.method == 'GET':
        a = request.args.get('a')
        b = request.args.get('b')
        return str(int(a)-int(b))


if __name__=="__main__":
    app.run(host='0.0.0.0', port=8174)

