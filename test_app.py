import unittest
import app

class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app

    def test_add(self):
        c = app.app.test_client()
        response = c.get('/add?a=5&b=3')
        self.assertEqual(int(response.get_data()), 8)

    def test_sub(self):
        c = app.app.test_client()
        response = c.get('/sub?a=5&b=3')
        self.assertEqual(int(response.get_data()), 2)


if __name__== "__main__":
    unittest.main()
